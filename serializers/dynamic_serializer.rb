class DynamicSerializer < ActiveModel::Serializer
  #
  # "Smart" attributes override.  This method will grab all of a models attributes and return them.
  # If attrs are passed in (this happens with 'fields' option in serializer), it will
  # only serialize those.
  #
  # Also, if the serializer defines a never_return method that returns an array of the
  # attributes (as strings - e.g., ['administrator', 'superuser', 'tenant_ids']),
  # it will omit those attributes.
  #
  def attributes(*attrs)

    all_attrs = object.attributes

    specific_attrs = attrs.first # honor fields args if present

    specific_attrs ? all_attrs.slice(*specific_attrs.map(&:to_s)) : all_attrs

    end
end