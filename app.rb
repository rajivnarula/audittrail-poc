require 'rubygems'
require 'sinatra'
require 'sinatra/activerecord'
require 'pry'
require 'sinatra/reloader'
require 'json_api_utils'
require "will_paginate"
require "will_paginate/active_record"
require "api-pagination"

require "./serializers/dynamic_serializer"

configure do

  ActiveModelSerializers.config.adapter = :json_api
  ActiveModelSerializers.config.key_transform = :unaltered
  ActiveModelSerializers.config.include_data_default = :if_sideloaded

  ApiPagination.configure do |config|
    # If you have both gems included, you can choose a paginator.
    config.paginator = :will_paginate # or :kaminari

    # By default, this is set to 'Total'
    config.total_header = 'X-Total'

    # By default, this is set to 'Per-Page'
    config.per_page_header = 'X-Page-Size'

    # Optional: set this to add a header with the current page number.
    config.page_header = 'X-Page'
  end

end

helpers do
  #
  # Get JSON API arguments
  #
  def parse_jsonapi_arguments
    # optional includes
    @japi_includes = params[:include].split(',') rescue []

    # optional fields
    @japi_fields = {}
    params[:fields].each_pair do |field_type, values|
      @japi_fields[field_type] = values.split(',')
    end if params[:fields].kind_of?(Hash)

    # set pagination default
    params[:per_page] =  params[:page][:size] rescue nil
    params[:page] =  params['page']['number'] rescue nil

  end

  # A weird hack for sinatra/pagination/jsonapi to work together
  def serialization_context_options
    Struct.new(:request_url, :query_parameters).new(
            request.path,
            env['rack.request.query_hash']
        )
  end

end

class AuditTrailEvent < ActiveRecord::Base
  validates :auditable_type, :auditable_id, :audit_trail_event_type_id, presence: true
end

class ErrorSerializer
  def self.serialize(errors, pointer, message = nil)
    return if errors.nil?

    if message
      {
        errors:
          [
            {
            source: { pointer: pointer },
            detail: message
          }
        ]
      }
    else
      json = {}
      new_hash = errors.to_hash(true).map do |k, v|
        v.map do |msg|
          { id: k, title: msg }
        end
      end.flatten
      json[:errors] = new_hash
      json
    end
  end
end

get '/' do
  content_type :json
  p AuditTrailEvent.all.to_json.as_json
end

get '/events' do
  # Support fields argument. E.G., http://localhost:4567/events?fields[audit_trail_events]=actor_name
  parse_jsonapi_arguments # fields, pagination
  # Support filtering via criteria http://localhost:4567/events?criteria[actor_name]=Kenneth%20Dornback&criteria[tenant_id]=1
  events = ApiPagination.paginate AuditTrailEvent.where(params[:criteria]), params
  ps = ActiveModelSerializers::SerializableResource.new(events, each_serializer: DynamicSerializer, meta: {},
        adapter: :json_api, fields: @japi_fields, serialization_context: serialization_context_options)
  ps.to_json
end

get '/admin' do
end

post '/audit_trail_events' do
  japi = JsonApiUtils.deserialize(JSON.parse(request.body.read))
  ate = japi.data.first
  attrs = ate['attributes']

  if attrs['auditable_owner'].present?
    auditable_owner_id = attrs['auditable_owner'].delete('id')
  end

  at = AuditTrailEvent.new(attrs)
  if at.save
    resp = JsonApiUtils.format_data("audit_trail_events", at.as_json)
    content_type 'application/vnd.api+json'
    body resp.to_json
    status 201
  else
    content_type 'application/vnd.api+json'
    body ErrorSerializer.serialize([], 'audit_trail_events', at.errors)
    status 500
  end
end

error do
  content_type 'application/vnd.api+json'
  body ErrorSerializer.serialize([], 'audit_trail_events', sinatra.error)
  status 500
end
