class CreateAuditTrailEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :audit_trail_events do |t|
      t.string :audit_trail_event_type_id
      t.integer :actor_id
      t.string :ip_address
      t.string :user_agent
      t.string :session_id
      t.text :details
      t.integer :tenant_id
      t.string :actor_name
      t.integer :parent_audit_trail_event_id
      t.string :auditable_type
      t.integer :auditable_id
      t.string :auditable_identifier
      t.integer :auditable_owner_id
      t.string :auditable_owner_name
      t.text :note
      t.string :attachment_file_name
      t.string :attachment_content_type
      t.integer :attachment_file_size
      t.datetime :attachment_updated_at
      t.string :email_to
      t.integer :email_to_user_id
      t.integer :email_to_account_id
      t.boolean :is_api_call
    end
    add_index :audit_trail_events, :actor_id
    add_index :audit_trail_events, :audit_trail_event_type_id
    add_index :audit_trail_events, :auditable_id
    add_index :audit_trail_events, :auditable_owner_id
    add_index :audit_trail_events, :auditable_type
    add_index :audit_trail_events, :email_to_account_id
    add_index :audit_trail_events, :email_to_user_id
    add_index :audit_trail_events, :ip_address
    add_index :audit_trail_events, :parent_audit_trail_event_id
    add_index :audit_trail_events, :session_id
    add_index :audit_trail_events, :tenant_id
  end
end
