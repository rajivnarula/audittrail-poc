# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170630151012) do

  create_table "audit_trail_events", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string "audit_trail_event_type_id"
    t.integer "actor_id"
    t.string "ip_address"
    t.string "user_agent"
    t.string "session_id"
    t.text "details"
    t.integer "tenant_id"
    t.string "actor_name"
    t.integer "parent_audit_trail_event_id"
    t.string "auditable_type"
    t.integer "auditable_id"
    t.string "auditable_identifier"
    t.integer "auditable_owner_id"
    t.string "auditable_owner_name"
    t.text "note"
    t.string "attachment_file_name"
    t.string "attachment_content_type"
    t.integer "attachment_file_size"
    t.datetime "attachment_updated_at"
    t.string "email_to"
    t.integer "email_to_user_id"
    t.integer "email_to_account_id"
    t.boolean "is_api_call"
    t.index ["actor_id"], name: "index_audit_trail_events_on_actor_id"
    t.index ["audit_trail_event_type_id"], name: "index_audit_trail_events_on_audit_trail_event_type_id"
    t.index ["auditable_id"], name: "index_audit_trail_events_on_auditable_id"
    t.index ["auditable_owner_id"], name: "index_audit_trail_events_on_auditable_owner_id"
    t.index ["auditable_type"], name: "index_audit_trail_events_on_auditable_type"
    t.index ["email_to_account_id"], name: "index_audit_trail_events_on_email_to_account_id"
    t.index ["email_to_user_id"], name: "index_audit_trail_events_on_email_to_user_id"
    t.index ["ip_address"], name: "index_audit_trail_events_on_ip_address"
    t.index ["parent_audit_trail_event_id"], name: "index_audit_trail_events_on_parent_audit_trail_event_id"
    t.index ["session_id"], name: "index_audit_trail_events_on_session_id"
    t.index ["tenant_id"], name: "index_audit_trail_events_on_tenant_id"
  end

end
